/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema.chaveiro;

import sistema.conexao.ConexaoBanco;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.UIManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
import sistema.caixa.TelaCaixa;
import sistema.funcionario.TelaFuncionários;
import sistema.os.TelaOS;
import sistema.os.Teste;
import sistema.produto.TelaProdutos;

public class TelaMenu extends javax.swing.JFrame {
    
    Connection conexao = null;
   
    public TelaMenu() {
        
        initComponents();
          this.setIconImage(new ImageIcon(getClass().getResource("/images/key.png")).getImage());
          conexao = ConexaoBanco.conector();
   
        //Look and Feel - Nimbus  
        try {

          UIManager.setLookAndFeel( "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        }
          catch (ClassNotFoundException ex) {
          java.util.logging.Logger.getLogger(TelaMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
          }catch (InstantiationException ex) {
          java.util.logging.Logger.getLogger(TelaMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

          } catch (IllegalAccessException ex) {
          java.util.logging.Logger.getLogger(TelaMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
          } catch (javax.swing.UnsupportedLookAndFeelException ex) {
          java.util.logging.Logger.getLogger(TelaMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
          }
                
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpMenu = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        btnCliente = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        btnFuncionario = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JToolBar.Separator();
        btnOS = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JToolBar.Separator();
        bntProdutos = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JToolBar.Separator();
        btnEncomenda = new javax.swing.JButton();
        jSeparator7 = new javax.swing.JToolBar.Separator();
        btnFornecedor = new javax.swing.JButton();
        jSeparator6 = new javax.swing.JToolBar.Separator();
        btnContas = new javax.swing.JButton();
        jSeparator8 = new javax.swing.JToolBar.Separator();
        btnCaixa = new javax.swing.JButton();
        jSeparator9 = new javax.swing.JToolBar.Separator();
        jButton10 = new javax.swing.JButton();
        jSeparator10 = new javax.swing.JToolBar.Separator();
        jdpMenu = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        miCliente = new javax.swing.JMenuItem();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem15 = new javax.swing.JMenuItem();
        itemOS = new javax.swing.JMenu();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        itemClientes = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SISCHA");
        setBackground(new java.awt.Color(204, 204, 204));
        setExtendedState(6);
        setIconImages(null);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jpMenu.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);
        jToolBar1.add(jSeparator1);

        btnCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/cliente6.png"))); // NOI18N
        btnCliente.setToolTipText("Clientes");
        btnCliente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCliente.setMaximumSize(new java.awt.Dimension(55, 55));
        btnCliente.setMinimumSize(new java.awt.Dimension(55, 55));
        btnCliente.setPreferredSize(new java.awt.Dimension(55, 55));
        btnCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClienteActionPerformed(evt);
            }
        });
        jToolBar1.add(btnCliente);
        jToolBar1.add(jSeparator2);

        btnFuncionario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/funcionario6.png"))); // NOI18N
        btnFuncionario.setToolTipText("Funcionários");
        btnFuncionario.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnFuncionario.setMaximumSize(new java.awt.Dimension(55, 55));
        btnFuncionario.setMinimumSize(new java.awt.Dimension(55, 55));
        btnFuncionario.setPreferredSize(new java.awt.Dimension(55, 55));
        btnFuncionario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFuncionarioActionPerformed(evt);
            }
        });
        jToolBar1.add(btnFuncionario);
        jToolBar1.add(jSeparator3);

        btnOS.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ordem de serviço7.png"))); // NOI18N
        btnOS.setToolTipText("Ordem de Serviços");
        btnOS.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnOS.setMaximumSize(new java.awt.Dimension(55, 55));
        btnOS.setMinimumSize(new java.awt.Dimension(55, 55));
        btnOS.setPreferredSize(new java.awt.Dimension(55, 55));
        btnOS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOSActionPerformed(evt);
            }
        });
        jToolBar1.add(btnOS);
        jToolBar1.add(jSeparator4);

        bntProdutos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/produto6.png"))); // NOI18N
        bntProdutos.setToolTipText("Produtos");
        bntProdutos.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bntProdutos.setMaximumSize(new java.awt.Dimension(55, 55));
        bntProdutos.setMinimumSize(new java.awt.Dimension(55, 55));
        bntProdutos.setPreferredSize(new java.awt.Dimension(55, 55));
        bntProdutos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bntProdutosActionPerformed(evt);
            }
        });
        jToolBar1.add(bntProdutos);
        jToolBar1.add(jSeparator5);

        btnEncomenda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/encomendas.png"))); // NOI18N
        btnEncomenda.setToolTipText("Encomendas");
        btnEncomenda.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEncomenda.setMaximumSize(new java.awt.Dimension(55, 55));
        btnEncomenda.setMinimumSize(new java.awt.Dimension(55, 55));
        btnEncomenda.setPreferredSize(new java.awt.Dimension(55, 55));
        btnEncomenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEncomendaActionPerformed(evt);
            }
        });
        jToolBar1.add(btnEncomenda);
        jToolBar1.add(jSeparator7);

        btnFornecedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/fornecedor6.png"))); // NOI18N
        btnFornecedor.setToolTipText("Fornecedores");
        btnFornecedor.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnFornecedor.setMaximumSize(new java.awt.Dimension(55, 55));
        btnFornecedor.setMinimumSize(new java.awt.Dimension(55, 55));
        btnFornecedor.setPreferredSize(new java.awt.Dimension(55, 55));
        btnFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFornecedorActionPerformed(evt);
            }
        });
        jToolBar1.add(btnFornecedor);
        jToolBar1.add(jSeparator6);

        btnContas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/contas.png"))); // NOI18N
        btnContas.setToolTipText("Contas");
        btnContas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnContas.setMaximumSize(new java.awt.Dimension(55, 55));
        btnContas.setMinimumSize(new java.awt.Dimension(55, 55));
        btnContas.setPreferredSize(new java.awt.Dimension(55, 55));
        btnContas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnContasActionPerformed(evt);
            }
        });
        jToolBar1.add(btnContas);
        jToolBar1.add(jSeparator8);

        btnCaixa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/caixa.png"))); // NOI18N
        btnCaixa.setToolTipText("Caixa");
        btnCaixa.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCaixa.setMaximumSize(new java.awt.Dimension(55, 55));
        btnCaixa.setMinimumSize(new java.awt.Dimension(55, 55));
        btnCaixa.setPreferredSize(new java.awt.Dimension(55, 55));
        btnCaixa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCaixaActionPerformed(evt);
            }
        });
        jToolBar1.add(btnCaixa);
        jToolBar1.add(jSeparator9);

        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/recibo.png"))); // NOI18N
        jButton10.setToolTipText("Recibo");
        jButton10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton10.setMaximumSize(new java.awt.Dimension(55, 55));
        jButton10.setMinimumSize(new java.awt.Dimension(55, 55));
        jButton10.setPreferredSize(new java.awt.Dimension(55, 55));
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton10);
        jToolBar1.add(jSeparator10);

        jdpMenu.setBackground(new java.awt.Color(153, 153, 153));

        javax.swing.GroupLayout jdpMenuLayout = new javax.swing.GroupLayout(jdpMenu);
        jdpMenu.setLayout(jdpMenuLayout);
        jdpMenuLayout.setHorizontalGroup(
            jdpMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jdpMenuLayout.setVerticalGroup(
            jdpMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 421, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jpMenuLayout = new javax.swing.GroupLayout(jpMenu);
        jpMenu.setLayout(jpMenuLayout);
        jpMenuLayout.setHorizontalGroup(
            jpMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpMenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 836, Short.MAX_VALUE)
                    .addComponent(jdpMenu))
                .addContainerGap())
        );
        jpMenuLayout.setVerticalGroup(
            jpMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpMenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdpMenu)
                .addContainerGap())
        );

        jMenu1.setText("Cadastrar");
        jMenu1.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        miCliente.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        miCliente.setText("Clientes");
        miCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miClienteActionPerformed(evt);
            }
        });
        jMenu1.add(miCliente);

        jMenuItem14.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jMenuItem14.setText("Funcionários");
        jMenu1.add(jMenuItem14);

        jMenuItem2.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jMenuItem2.setText("Ordem de Serviços");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem5.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jMenuItem5.setText("Produtos");
        jMenu1.add(jMenuItem5);

        jMenuItem3.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jMenuItem3.setText("Fornecedores");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuItem4.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jMenuItem4.setText("Encomendas");
        jMenu1.add(jMenuItem4);

        jMenuItem6.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jMenuItem6.setText("Contas");
        jMenu1.add(jMenuItem6);

        jMenuItem15.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jMenuItem15.setText("Recibos");
        jMenu1.add(jMenuItem15);

        jMenuBar1.add(jMenu1);

        itemOS.setText("Retatórios");
        itemOS.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        jMenuItem7.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jMenuItem7.setText("Ordem de Serviço");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        itemOS.add(jMenuItem7);

        jMenuItem8.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jMenuItem8.setText("Produtos");
        itemOS.add(jMenuItem8);

        jMenuItem9.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jMenuItem9.setText("Caixa");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        itemOS.add(jMenuItem9);

        itemClientes.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        itemClientes.setText("Clientes");
        itemClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemClientesActionPerformed(evt);
            }
        });
        itemOS.add(itemClientes);

        jMenuItem10.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jMenuItem10.setText("Contas");
        itemOS.add(jMenuItem10);

        jMenuItem11.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jMenuItem11.setText("Encomendas");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        itemOS.add(jMenuItem11);

        jMenuBar1.add(itemOS);

        jMenu3.setText("Ajuda");
        jMenu3.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        jMenuItem13.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jMenuItem13.setText("Suporte");
        jMenu3.add(jMenuItem13);

        jMenuItem12.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jMenuItem12.setText("Sobre");
        jMenu3.add(jMenuItem12);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jpMenu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jpMenu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void miClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miClienteActionPerformed
             
    }//GEN-LAST:event_miClienteActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void btnFuncionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFuncionarioActionPerformed
        // TODO add your handling code here:
         TelaFuncionários cli1 = new  TelaFuncionários();
         jdpMenu.add(cli1);
         //cli1.setPosicao();
         cli1.setVisible(true);
       
    }//GEN-LAST:event_btnFuncionarioActionPerformed
   
    private void btnClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClienteActionPerformed
         TelaClientes cli1 = new  TelaClientes();
         jdpMenu.add(cli1);
         //cli1.setPosicao();
         cli1.setVisible(true);

    }//GEN-LAST:event_btnClienteActionPerformed

    private void btnEncomendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEncomendaActionPerformed

    }//GEN-LAST:event_btnEncomendaActionPerformed

    private void btnFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFornecedorActionPerformed
        // TODO add your handling code here:
         TelaFornecedores tela = new  TelaFornecedores();
         jdpMenu.add(tela);
         tela.setVisible(true);
        
    }//GEN-LAST:event_btnFornecedorActionPerformed

    private void bntProdutosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bntProdutosActionPerformed
        TelaProdutos tela = new  TelaProdutos();
         jdpMenu.add(tela);
         tela.setVisible(true);
        
    }//GEN-LAST:event_bntProdutosActionPerformed

    private void btnOSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOSActionPerformed
         TelaOS tela = new  TelaOS();
         jdpMenu.add(tela);
         tela.setVisible(true);
    }//GEN-LAST:event_btnOSActionPerformed

    private void btnContasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnContasActionPerformed
        // TODO add your handling code here:
         TelaContas cli1 = new  TelaContas();
         jdpMenu.add(cli1);
         cli1.setVisible(true);
    }//GEN-LAST:event_btnContasActionPerformed

    private void btnCaixaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCaixaActionPerformed
        // TODO add your handling code here:
         TelaCaixa tela = new  TelaCaixa();
         jdpMenu.add(tela);
         tela.setPosicao();
         tela.setVisible(true);
        
    }//GEN-LAST:event_btnCaixaActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        TelaServicos s = new TelaServicos();
        jdpMenu.add(s);
        s.setVisible(true);
        
        
    }//GEN-LAST:event_jButton10ActionPerformed

    private void itemClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemClientesActionPerformed
        // gerando um relatório de clientes
        int confirma = JOptionPane.showConfirmDialog(null, "Confirma a impressão deste relatório?",
                "Atenção",JOptionPane.YES_NO_OPTION);
        if(confirma == JOptionPane.YES_OPTION){
            try {
                JasperPrint print = JasperFillManager.fillReport("C:/reports/clientes.jasper",null,conexao);
                JasperViewer.viewReport(print,false);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }//GEN-LAST:event_itemClientesActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        
    
    }//GEN-LAST:event_formWindowOpened


    public static void main(String args[]) {
   
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaMenu().setVisible(true);
                
            }
        });
        
  
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bntProdutos;
    private javax.swing.JButton btnCaixa;
    private javax.swing.JButton btnCliente;
    private javax.swing.JButton btnContas;
    private javax.swing.JButton btnEncomenda;
    private javax.swing.JButton btnFornecedor;
    private javax.swing.JButton btnFuncionario;
    private javax.swing.JButton btnOS;
    private javax.swing.JMenuItem itemClientes;
    private javax.swing.JMenu itemOS;
    private javax.swing.JButton jButton10;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator10;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JToolBar.Separator jSeparator3;
    private javax.swing.JToolBar.Separator jSeparator4;
    private javax.swing.JToolBar.Separator jSeparator5;
    private javax.swing.JToolBar.Separator jSeparator6;
    private javax.swing.JToolBar.Separator jSeparator7;
    private javax.swing.JToolBar.Separator jSeparator8;
    private javax.swing.JToolBar.Separator jSeparator9;
    private javax.swing.JToolBar jToolBar1;
    public static javax.swing.JDesktopPane jdpMenu;
    private javax.swing.JPanel jpMenu;
    private javax.swing.JMenuItem miCliente;
    // End of variables declaration//GEN-END:variables
}

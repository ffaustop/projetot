/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema.os;

import java.awt.Font;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import net.proteanit.sql.DbUtils;
import sistema.conexao.ConexaoBanco;
import static sistema.chaveiro.TelaMenu.jdpMenu;
import sistema.chaveiro.TelaClientes;
import sistema.produto.TelaProdutos;
import sistema.chaveiro.TelaServicos;

/**
 *
 * @author Pedro
 */

public class TelaOS extends javax.swing.JInternalFrame {
    
    Connection conexao = null;
    PreparedStatement pst = null;
    ResultSet rs = null;
    private String tipo;
    
    public String formataDinheiro(double valor) {
    return "R$ " + String.format("%.2f", valor);
}
  private void adicionar() {
        String sql = "INSERT INTO os(os_situacao, os_quantidade, ser_cod, cli_cod, fun_cod, prod_cod, os_forma_pagamento, os_vencimento, os_tipo, os_valor_total) VALUES (?,?,?,?,?,?,?,?,?,?)";
        try {
            pst = conexao.prepareStatement(sql);      
            pst.setString(1, boxSituacao.getSelectedItem().toString());
            pst.setString(2, spinQtdS.getValue().toString());
            pst.setInt(3, boxServicos.getSelectedIndex());
            pst.setInt(4, boxClientes.getSelectedIndex());
            pst.setInt(5, boxFuncionarios.getSelectedIndex());
            pst.setInt(6, boxProdutos.getSelectedIndex());
            pst.setString(7, boxPagamento.getSelectedItem().toString());
            pst.setString(8, txtVencimento.getText());
            pst.setString(9, tipo);
            pst.setString(10, lol.getText());

            
            if (boxServicos.getSelectedItem().equals("") || boxClientes.getSelectedIndex() == 0 ){
                JOptionPane.showMessageDialog(null, "'Nome' e 'CPF' são obrigatórios!");

            } else {
                int adicionado = pst.executeUpdate();
                if (adicionado > 0) {
                    JOptionPane.showMessageDialog(null, "Dados cadastrados com sucesso!");
                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
     
    private void calculaTotalServicos() {
        Double Orcamento = 0.00;
        for ( int i = 0; i < tblServicos.getRowCount(); i++){
            Orcamento += Double.parseDouble( tblServicos.getValueAt(i, 3).toString());
        }
 
            lolS.setValue(Orcamento);
    }

    private void calculaTotalProdutos() {
        Double Orcamento = 0.00;
        for ( int i = 0; i < tblProdutos.getRowCount(); i++){
            Orcamento += Double.parseDouble( tblProdutos.getValueAt(i, 3).toString());
        }
         //return Orcamento.toString(); 
        lolP.setValue(Orcamento);
    }
    
    private void calculaTotal(){
        Double s = (Double) lolS.getValue();
        Double p = (Double) lolP.getValue();
        if (lolP.getValue() == null) {
                        lol.setValue(s);
        }else{
         lol.setValue(s+p);

        }
   
       
    }
    
    public void carregaBoxServicos() {
        try {
       
            boxServicos.addItem("Selecione...");
            Connection conn;
            conn = ConexaoBanco.conector();//classse de conexao com banco
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT servicos.ser_descricao, servicos.ser_cod FROM servicos ORDER BY ser_cod, ser_descricao");
            while (rs.next()) {
                boxServicos.addItem(rs.getString("ser_descricao"));
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,
                    "Ocorreu erro ao carregar a Combo Box", "Erro",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void carregarBoxProdutos() {
        try {                               
            boxProdutos.addItem("Selecione...");
            Connection conn;
            conn = ConexaoBanco.conector();//classse de conexao com banco
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT produtos.prod_cod, produtos.prod_marca FROM produtos ORDER BY prod_cod, prod_marca");
            while (rs.next()) {
                boxProdutos.addItem(rs.getString("prod_marca"));
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,
                    "Ocorreu erro ao carregar a Combo Box "+e, "Erro ",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    
     private void carregarBoxClientes() {
       try {
            boxClientes.addItem("Selecione...");
            Connection conn;
            conn = ConexaoBanco.conector();//classse de conexao com banco
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT clientes.cli_cod, clientes.cli_nome FROM clientes ORDER BY cli_cod, cli_nome");
            while (rs.next()) {
                boxClientes.addItem(rs.getString("cli_nome"));
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,
                    "Ocorreu erro ao carregar a Combo Box "+e, "Erro ",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

       private void carregarBoxFuncionarios() {
       try {
            boxFuncionarios.addItem("Selecione...");
            Connection conn;
            conn = ConexaoBanco.conector();//classse de conexao com banco
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT funcionarios.fun_cod, funcionarios.fun_nome FROM funcionarios ORDER BY fun_cod, fun_nome");
            while (rs.next()) {
                boxFuncionarios.addItem(rs.getString("fun_nome"));
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,
                    "Ocorreu erro ao carregar a Combo Box "+e, "Erro ",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    //Método para pesquisar a numeração da chave.

    private void pesquisarNumeracao() {
        String n = JOptionPane.showInputDialog("Pesquisar numeração:");
        String sql = "select * from produtos where prod_numeracao= " + n;
        try {
            pst = conexao.prepareStatement(sql);

            rs = pst.executeQuery();
            if (rs.next()) {
                boxProdutos.setSelectedItem(rs.getString(4));
            } else {
                JOptionPane.showMessageDialog(null, "Numeração não cadastrada!");
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    private void setarPrecoServicos() {
        String box = boxServicos.getSelectedItem().toString();
        String sql = "select * from servicos where ser_descricao= '" + box + "'";
        try {
            pst = conexao.prepareStatement(sql);

            rs = pst.executeQuery();
            
            if (rs.next()) {
                txtServicoPreco.setValue(rs.getDouble(3));
            }
            if (boxServicos.getSelectedIndex() == 0) {
                txtServicoPreco.setText(null);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    private void setarPrecoProdutos() {
        String box = boxProdutos.getSelectedItem().toString();
        String sql = "select * from produtos where prod_marca= '" + box + "'";
        try {
            pst = conexao.prepareStatement(sql);

            rs = pst.executeQuery();

            if (rs.next()) {
                txtProdutoPreco.setValue(rs.getDouble(5));
            }
            if (boxProdutos.getSelectedIndex() == 0) {
                txtProdutoPreco.setValue(null);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    //método para habilitar quanto serviço possui produto, como cópia ou confecção que precisa de chave.

    private void servicoProduto() {
         
        if (boxServicos.getSelectedIndex() == 3) {
            boxSituacao.setSelectedIndex(1);
        } else {
            boxSituacao.setSelectedIndex(0);
        }
        if (boxServicos.getSelectedIndex() == 1 || boxServicos.getSelectedIndex() == 2 || boxServicos.getSelectedIndex() == 3) {
            panProduto.setVisible(true);
            lolP.setEnabled(true);
        } else {
            panProduto.setVisible(false);
           
        }
    }
    
    private void vincularTabela(){
         String serv = boxServicos.getSelectedItem().toString();
        String qtdS = spinQtdS.getValue().toString();
        String preco = txtServicoPreco.getText();
           // DefaultTableModel val = (DefaultTableModel) tblOS.getModel(); 
       
   if (boxServicos.getSelectedIndex() != 0) {          
    } 
    }
   
    //Método para alterar dados
  
    private void data(){
        Date hoje = new Date();
        SimpleDateFormat df;
        df = new SimpleDateFormat("dd/MM/yyyy");
        txtVencimento.setText(df.format(hoje));
        }
    private void fontTabelasOS(){
        JTableHeader cabecalho = tblServicos.getTableHeader();
        JTableHeader cabecalho2 = tblProdutos.getTableHeader();
        cabecalho.setFont(new java.awt.Font("Tahoma", 0, 15));
        cabecalho2.setFont(new java.awt.Font("Tahoma", 0, 15));
    }
   
    public TelaOS() {
        initComponents();
        conexao = ConexaoBanco.conector();
        System.out.println(conexao);
        panProduto.setVisible(false);
        carregaBoxServicos();
        carregarBoxProdutos();
        carregarBoxClientes();
        carregarBoxFuncionarios();
        data();
        fontTabelasOS();

    }
  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        radioTipo = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        panPagamento = new javax.swing.JPanel();
        boxFuncionarios = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        boxPagamento = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtVencimento = new javax.swing.JFormattedTextField();
        jpBotoes = new javax.swing.JPanel();
        btnSalvar = new javax.swing.JButton();
        jButton20 = new javax.swing.JButton();
        jLabel19 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButton21 = new javax.swing.JButton();
        jLabel20 = new javax.swing.JLabel();
        jButton22 = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        panServicos = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtCod = new javax.swing.JTextField();
        boxServicos = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        btnOs = new javax.swing.JButton();
        txtData = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        boxSituacao = new javax.swing.JComboBox();
        jLabel13 = new javax.swing.JLabel();
        radioOc = new javax.swing.JRadioButton();
        radioOs = new javax.swing.JRadioButton();
        spinQtdS = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();
        txtServicoPreco = new javax.swing.JFormattedTextField();
        panProduto = new javax.swing.JPanel();
        boxProdutos = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        btnProduto = new javax.swing.JButton();
        txtQtdP = new javax.swing.JSpinner();
        jLabel4 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        btnProdutoAdd = new javax.swing.JButton();
        txtProdutoPreco = new javax.swing.JFormattedTextField();
        panOS = new javax.swing.JTabbedPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblServicos = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblProdutos = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        lolS = new javax.swing.JFormattedTextField();
        lolP = new javax.swing.JFormattedTextField();
        jLabel15 = new javax.swing.JLabel();
        lol = new javax.swing.JFormattedTextField();
        boxClientes = new javax.swing.JComboBox();
        btnCliente = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Ordem de Serviços");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jPanel2MouseMoved(evt);
            }
        });

        panPagamento.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        boxFuncionarios.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        boxFuncionarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boxFuncionariosActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel8.setText("Funcionário:");

        boxPagamento.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        boxPagamento.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "à Vista", "Cheque" }));
        boxPagamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boxPagamentoActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel9.setText("Forma de Pagamento:");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel10.setText("Vencimento:");

        try {
            txtVencimento.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtVencimento.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        txtVencimento.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtVencimentoFocusLost(evt);
            }
        });

        javax.swing.GroupLayout panPagamentoLayout = new javax.swing.GroupLayout(panPagamento);
        panPagamento.setLayout(panPagamentoLayout);
        panPagamentoLayout.setHorizontalGroup(
            panPagamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panPagamentoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panPagamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(boxFuncionarios, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panPagamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boxPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panPagamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addComponent(txtVencimento, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18))
        );
        panPagamentoLayout.setVerticalGroup(
            panPagamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panPagamentoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panPagamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panPagamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel10)
                        .addGroup(panPagamentoLayout.createSequentialGroup()
                            .addGap(21, 21, 21)
                            .addComponent(txtVencimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panPagamentoLayout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(2, 2, 2)
                        .addComponent(boxPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panPagamentoLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(2, 2, 2)
                        .addComponent(boxFuncionarios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/salvar.png"))); // NOI18N
        btnSalvar.setMaximumSize(new java.awt.Dimension(90, 30));
        btnSalvar.setMinimumSize(new java.awt.Dimension(90, 30));
        btnSalvar.setPreferredSize(new java.awt.Dimension(90, 30));
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        jButton20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/finalizar.png"))); // NOI18N
        jButton20.setMaximumSize(new java.awt.Dimension(90, 30));
        jButton20.setMinimumSize(new java.awt.Dimension(90, 30));
        jButton20.setPreferredSize(new java.awt.Dimension(90, 30));

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel19.setText("Imprimir");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Gravar Dados");

        jButton21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/imprimir.png"))); // NOI18N
        jButton21.setMaximumSize(new java.awt.Dimension(90, 30));
        jButton21.setMinimumSize(new java.awt.Dimension(90, 30));
        jButton21.setPreferredSize(new java.awt.Dimension(90, 30));

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel20.setText("Finalizar ");

        jButton22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/fechar.png"))); // NOI18N
        jButton22.setMaximumSize(new java.awt.Dimension(90, 30));
        jButton22.setMinimumSize(new java.awt.Dimension(90, 30));
        jButton22.setPreferredSize(new java.awt.Dimension(90, 30));

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel21.setText("Fechar");

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel17.setText("Orçamento/O.S");

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel22.setText("Orçamento/O.S");

        javax.swing.GroupLayout jpBotoesLayout = new javax.swing.GroupLayout(jpBotoes);
        jpBotoes.setLayout(jpBotoesLayout);
        jpBotoesLayout.setHorizontalGroup(
            jpBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpBotoesLayout.createSequentialGroup()
                .addGroup(jpBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpBotoesLayout.createSequentialGroup()
                        .addGroup(jpBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23)
                        .addGroup(jpBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpBotoesLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel20))
                            .addComponent(jButton20, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jpBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpBotoesLayout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addComponent(jLabel19))
                            .addGroup(jpBotoesLayout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(jButton21, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jpBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpBotoesLayout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(jButton22, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jpBotoesLayout.createSequentialGroup()
                                .addGap(44, 44, 44)
                                .addComponent(jLabel21))))
                    .addGroup(jpBotoesLayout.createSequentialGroup()
                        .addGap(92, 92, 92)
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel22)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpBotoesLayout.setVerticalGroup(
            jpBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpBotoesLayout.createSequentialGroup()
                .addGroup(jpBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpBotoesLayout.createSequentialGroup()
                        .addGroup(jpBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton22, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jpBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel3)
                                .addComponent(jLabel20))
                            .addComponent(jLabel21))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jpBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(jLabel22)))
                    .addGroup(jpBotoesLayout.createSequentialGroup()
                        .addGroup(jpBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton21, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton20, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel19)))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        panServicos.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Serviço"));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel1.setText("Nº OS:");

        txtCod.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        txtCod.setEnabled(false);
        txtCod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCodActionPerformed(evt);
            }
        });

        boxServicos.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        boxServicos.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                boxServicosItemStateChanged(evt);
            }
        });
        boxServicos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boxServicosActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel7.setText("Serviço:");

        btnOs.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/adicionar1.png"))); // NOI18N
        btnOs.setMaximumSize(new java.awt.Dimension(30, 30));
        btnOs.setMinimumSize(new java.awt.Dimension(30, 30));
        btnOs.setPreferredSize(new java.awt.Dimension(30, 30));
        btnOs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOsActionPerformed(evt);
            }
        });

        txtData.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        txtData.setEnabled(false);
        txtData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDataActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel2.setText("Data:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel5.setText("Preço:");

        boxSituacao.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        boxSituacao.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Finalizado", "Em andamento", "Em estoque" }));
        boxSituacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boxSituacaoActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel13.setText("Situação:");

        radioTipo.add(radioOc);
        radioOc.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        radioOc.setText("Orçamento");
        radioOc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioOcActionPerformed(evt);
            }
        });

        radioTipo.add(radioOs);
        radioOs.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        radioOs.setText("Ordem de Serviço");
        radioOs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioOsActionPerformed(evt);
            }
        });

        spinQtdS.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        spinQtdS.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10000, 1));
        spinQtdS.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spinQtdSStateChanged(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel6.setText("Qtd.:");

        txtServicoPreco.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getCurrencyInstance())));
        txtServicoPreco.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        javax.swing.GroupLayout panServicosLayout = new javax.swing.GroupLayout(panServicos);
        panServicos.setLayout(panServicosLayout);
        panServicosLayout.setHorizontalGroup(
            panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panServicosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panServicosLayout.createSequentialGroup()
                        .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(txtCod, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panServicosLayout.createSequentialGroup()
                                .addComponent(boxServicos, javax.swing.GroupLayout.PREFERRED_SIZE, 394, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnOs, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panServicosLayout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(0, 30, Short.MAX_VALUE))
                            .addComponent(spinQtdS, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                    .addGroup(panServicosLayout.createSequentialGroup()
                        .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panServicosLayout.createSequentialGroup()
                                .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(txtServicoPreco, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel13)
                                    .addComponent(boxSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(panServicosLayout.createSequentialGroup()
                                .addComponent(radioOc)
                                .addGap(18, 18, 18)
                                .addComponent(radioOs)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panServicosLayout.setVerticalGroup(
            panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panServicosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panServicosLayout.createSequentialGroup()
                        .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panServicosLayout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(spinQtdS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(15, 15, 15))
                            .addGroup(panServicosLayout.createSequentialGroup()
                                .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel7))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(txtCod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(boxServicos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(btnOs, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panServicosLayout.createSequentialGroup()
                                .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtServicoPreco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabel13)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panServicosLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(boxSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(panServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioOc)
                    .addComponent(radioOs))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        boxProdutos.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        boxProdutos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                boxProdutosMouseClicked(evt);
            }
        });
        boxProdutos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boxProdutosActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel12.setText("Produto:");

        btnProduto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/buscar2.png"))); // NOI18N
        btnProduto.setMaximumSize(new java.awt.Dimension(30, 30));
        btnProduto.setMinimumSize(new java.awt.Dimension(30, 30));
        btnProduto.setPreferredSize(new java.awt.Dimension(30, 30));
        btnProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProdutoActionPerformed(evt);
            }
        });

        txtQtdP.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        txtQtdP.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10000, 1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel4.setText("Qtd.:");

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel14.setText("Preço:");

        btnProdutoAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/adicionar1.png"))); // NOI18N
        btnProdutoAdd.setMaximumSize(new java.awt.Dimension(30, 30));
        btnProdutoAdd.setMinimumSize(new java.awt.Dimension(30, 30));
        btnProdutoAdd.setPreferredSize(new java.awt.Dimension(30, 30));
        btnProdutoAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProdutoAddActionPerformed(evt);
            }
        });

        txtProdutoPreco.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getCurrencyInstance())));
        txtProdutoPreco.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        javax.swing.GroupLayout panProdutoLayout = new javax.swing.GroupLayout(panProduto);
        panProduto.setLayout(panProdutoLayout);
        panProdutoLayout.setHorizontalGroup(
            panProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panProdutoLayout.createSequentialGroup()
                .addGroup(panProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addGroup(panProdutoLayout.createSequentialGroup()
                        .addComponent(boxProdutos, javax.swing.GroupLayout.PREFERRED_SIZE, 326, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnProdutoAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(panProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panProdutoLayout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(jLabel14))
                    .addGroup(panProdutoLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(txtProdutoPreco, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(panProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panProdutoLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(0, 35, Short.MAX_VALUE))
                    .addGroup(panProdutoLayout.createSequentialGroup()
                        .addComponent(txtQtdP, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        panProdutoLayout.setVerticalGroup(
            panProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panProdutoLayout.createSequentialGroup()
                .addGroup(panProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(boxProdutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnProdutoAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGroup(panProdutoLayout.createSequentialGroup()
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtQtdP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProdutoPreco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        panOS.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        tblServicos.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        tblServicos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Descrição", "Quantidade", "Preço", "Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblServicos.addContainerListener(new java.awt.event.ContainerAdapter() {
            public void componentAdded(java.awt.event.ContainerEvent evt) {
                tblServicosComponentAdded(evt);
            }
        });
        jScrollPane2.setViewportView(tblServicos);

        panOS.addTab("Serviços", jScrollPane2);

        tblProdutos.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        tblProdutos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Descrição", "Quantidade", "Preço", "Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(tblProdutos);

        panOS.addTab("Produtos", jScrollPane3);

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel16.setText("Total Serviços:");

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel18.setText("Total Produtos:");

        lolS.setEditable(false);
        lolS.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getCurrencyInstance())));
        lolS.setText("R$ 0,00");
        lolS.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        lolP.setEditable(false);
        lolP.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getCurrencyInstance())));
        lolP.setText("R$ 0,00");
        lolP.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel15.setText("Total a Pagar:");

        lol.setEditable(false);
        lol.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getCurrencyInstance())));
        lol.setText("R$ 0,00");
        lol.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        lol.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lolActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lolS, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lolP)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(jLabel18)
                            .addComponent(jLabel15))
                        .addGap(0, 370, Short.MAX_VALUE))
                    .addComponent(lol))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lolS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lolP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        boxClientes.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        boxClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                boxClientesMouseClicked(evt);
            }
        });
        boxClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boxClientesActionPerformed(evt);
            }
        });

        btnCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/buscar2.png"))); // NOI18N
        btnCliente.setMaximumSize(new java.awt.Dimension(30, 30));
        btnCliente.setMinimumSize(new java.awt.Dimension(30, 30));
        btnCliente.setPreferredSize(new java.awt.Dimension(30, 30));
        btnCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClienteActionPerformed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel11.setText("Cliente:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(panServicos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(panPagamento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jpBotoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel11)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(boxClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 326, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(panOS, javax.swing.GroupLayout.PREFERRED_SIZE, 599, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(12, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(jpBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(panPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(panServicos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(boxClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(panProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)))
                .addGap(1, 1, 1)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panOS, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProdutoActionPerformed
        pesquisarNumeracao();
    }//GEN-LAST:event_btnProdutoActionPerformed

    private void btnOsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOsActionPerformed
          //Contador de cliques  
        
    
        String serv = boxServicos.getSelectedItem().toString();
         int qtd = (int) spinQtdS.getValue();
        double preco = (double) txtServicoPreco.getValue();
        
        double calculoTotal = qtd * preco;
        double ct = calculoTotal;
        
       
       DefaultTableModel t = (DefaultTableModel) tblServicos.getModel();
       Object[] dados = {serv,qtd,preco,calculoTotal};
        
        
        if (serv.equals("")) {
        JOptionPane.showMessageDialog(null, "Campo em branco!");
        } else {
            t.addRow(dados);
        }
        
        calculaTotalServicos();
        calculaTotal();
       
    }//GEN-LAST:event_btnOsActionPerformed
   
    private void boxSituacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boxSituacaoActionPerformed

    }//GEN-LAST:event_boxSituacaoActionPerformed

    private void boxProdutosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boxProdutosActionPerformed
        setarPrecoProdutos();
 
    }//GEN-LAST:event_boxProdutosActionPerformed

    private void txtDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDataActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDataActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        adicionar();

    }//GEN-LAST:event_btnSalvarActionPerformed

    private void boxPagamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boxPagamentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_boxPagamentoActionPerformed

    private void boxFuncionariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boxFuncionariosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_boxFuncionariosActionPerformed

    private void boxServicosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boxServicosActionPerformed
        setarPrecoServicos();
        servicoProduto();
    }//GEN-LAST:event_boxServicosActionPerformed

    private void txtCodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodActionPerformed

    private void boxServicosItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_boxServicosItemStateChanged

    }//GEN-LAST:event_boxServicosItemStateChanged

    private void boxProdutosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_boxProdutosMouseClicked

        
    }//GEN-LAST:event_boxProdutosMouseClicked

    private void btnProdutoAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProdutoAddActionPerformed
        String prod = boxProdutos.getSelectedItem().toString();
        int qtd = (int) txtQtdP.getValue();
        double preco = (double) txtProdutoPreco.getValue();
        double total = (qtd * preco);
       
        DefaultTableModel t = (DefaultTableModel) tblProdutos.getModel();
        Object[] dados = {prod,qtd,preco,total};
         if (prod.equals("")) {
            JOptionPane.showMessageDialog(null, "Campo em branco!");
        } else {
            t.addRow(dados);
        }
     calculaTotalProdutos();
         calculaTotal();
         
         
    }//GEN-LAST:event_btnProdutoAddActionPerformed

    private void spinQtdSStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spinQtdSStateChanged
      
    }//GEN-LAST:event_spinQtdSStateChanged

    private void tblServicosComponentAdded(java.awt.event.ContainerEvent evt) {//GEN-FIRST:event_tblServicosComponentAdded
    
    }//GEN-LAST:event_tblServicosComponentAdded

    private void jPanel2MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MouseMoved

    }//GEN-LAST:event_jPanel2MouseMoved

    private void txtVencimentoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtVencimentoFocusLost
        String s = txtVencimento.getText();
        DateFormat df = new SimpleDateFormat ("dd/MM/yyyy");
        df.setLenient (false); 
        try {
        df.parse (s);
        txtVencimento.setText(s);
        } catch (ParseException ex) {
          JOptionPane.showMessageDialog(null, "Formato de data inválido!");
          txtVencimento.setValue(null);
          txtVencimento.requestFocus();
        }     
    }//GEN-LAST:event_txtVencimentoFocusLost

    private void lolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lolActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lolActionPerformed

    private void boxClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_boxClientesMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_boxClientesMouseClicked

    private void boxClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boxClientesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_boxClientesActionPerformed

    private void btnClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClienteActionPerformed
        Teste t = new Teste();
        jdpMenu.add(t);
         t.setVisible(true);
    }//GEN-LAST:event_btnClienteActionPerformed

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        radioOs.setSelected(true); 
        tipo = "Ordem de Serviço";
        boxProdutos.setSelectedIndex(1);
        
    }//GEN-LAST:event_formInternalFrameOpened

    private void radioOcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioOcActionPerformed
        tipo = "Orçamento";
    }//GEN-LAST:event_radioOcActionPerformed

    private void radioOsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioOsActionPerformed
       tipo = "Ordem de Serviço";
    }//GEN-LAST:event_radioOsActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JComboBox boxClientes;
    private javax.swing.JComboBox boxFuncionarios;
    private javax.swing.JComboBox boxPagamento;
    private javax.swing.JComboBox boxProdutos;
    private javax.swing.JComboBox boxServicos;
    private javax.swing.JComboBox boxSituacao;
    private javax.swing.JButton btnCliente;
    private javax.swing.JButton btnOs;
    private javax.swing.JButton btnProduto;
    private javax.swing.JButton btnProdutoAdd;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JButton jButton20;
    private javax.swing.JButton jButton21;
    private javax.swing.JButton jButton22;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPanel jpBotoes;
    private javax.swing.JFormattedTextField lol;
    private javax.swing.JFormattedTextField lolP;
    private javax.swing.JFormattedTextField lolS;
    private javax.swing.JTabbedPane panOS;
    private javax.swing.JPanel panPagamento;
    private javax.swing.JPanel panProduto;
    private javax.swing.JPanel panServicos;
    private javax.swing.JRadioButton radioOc;
    private javax.swing.JRadioButton radioOs;
    private javax.swing.ButtonGroup radioTipo;
    private javax.swing.JSpinner spinQtdS;
    private javax.swing.JTable tblProdutos;
    private javax.swing.JTable tblServicos;
    private javax.swing.JTextField txtCod;
    private javax.swing.JTextField txtData;
    private javax.swing.JFormattedTextField txtProdutoPreco;
    private javax.swing.JSpinner txtQtdP;
    private javax.swing.JFormattedTextField txtServicoPreco;
    private javax.swing.JFormattedTextField txtVencimento;
    // End of variables declaration//GEN-END:variables
}

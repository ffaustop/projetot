package sistema.conexao;

import java.sql.*;


/**
 *
 * @author e8858240
 */

public class ConexaoBanco {

    public static Connection conector() {
        java.sql.Connection conexao = null;
        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/chaveiro";
        String user = "root";
        String password = "";
        //Estabelecendo a conexão com o banco.
        try {
            Class.forName(driver);
            conexao = DriverManager.getConnection(url, user, password);
            return conexao;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

}
